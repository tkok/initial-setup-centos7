# initial-setup-centos7
Ansible playbook for setup CentOS7 in initial.  
It contains some roles minimum required at first which are:
- packages
- ntp
- selinux
- timezone
- locale
- firewall

## Usage
```
# Make hosts file based on hosts.default
cp hosts.default hosts

# Execute (with --check at first is better.)
ansible-playbook -i hosts main.yml
```

## Test
```
# Ping
ansible -i hosts {host} -m ping

# Dry run
ansible-playbook -i hosts mail.yml --check
```